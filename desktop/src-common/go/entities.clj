(ns go.entities
  (:require [play-clj.g2d :refer :all]))


(defn create-textures!
  [tilesize]
  (let [sheet (texture "tiles.png")
        tiles (texture! sheet :split tilesize tilesize)]
    {:corner-tl (texture (aget tiles 0 0))
     :corner-tr (texture (aget tiles 0 3))
     :corner-bl (texture (aget tiles 2 0))
     :corner-br (texture (aget tiles 2 3))
     :side-t    (texture (aget tiles 0 1))
     :side-b    (texture (aget tiles 2 1))
     :side-l    (texture (aget tiles 1 0))
     :side-r    (texture (aget tiles 1 3))
     :inside    (texture (aget tiles 1 1))
     :marked    (texture (aget tiles 1 2))
     :cursor-w  (texture (aget tiles 3 0))
     :cursor-b  (texture (aget tiles 3 1))
     :stone-w   (texture (aget tiles 3 2))
     :stone-b   (texture (aget tiles 3 3))}))

(defn create-board
  [size textures]
  (let [init-board (for [x (range 0 size)
                         y (range 0 size)]
                        [x y])
        max-pos    (dec size) ]
    (->> (map (fn [[x y]]
           (cond
             (= x y 0)
               (assoc (:corner-bl textures) :x x :y y)
             (= x y max-pos)
               (assoc (:corner-tr textures) :x x :y y)
             (and (= x 0) (= y max-pos))
               (assoc (:corner-tl textures) :x x :y y)
             (and (= x max-pos) (= y 0))
               (assoc (:corner-br textures) :x x :y y)
             (= x 0)
               (assoc (:side-l textures) :x x :y y)
             (= x max-pos)
               (assoc (:side-r textures) :x x :y y)
             (= y 0)
               (assoc (:side-b textures) :x x :y y)
             (= y max-pos)
               (assoc (:side-t textures) :x x :y y)
             (= x y (/ max-pos 2))
               (assoc (:marked textures) :x x :y y)
             :else
               (assoc (:inside textures) :x x :y y ))
           ) init-board)
         (into []))))




(defn new-black-stone
  [textures x y]
    (assoc (:stone-b textures) :x x :y y))

(defn new-white-stone
  [textures x y]
    (assoc (:stone-w textures) :x x :y y))

(defn new-black-cursor
  [textures x y]
    (assoc (:cursor-b textures) :x x :y y))

(defn new-white-cursor
  [textures x y]
    (assoc (:cursor-w textures) :x x :y y))




(defn new-cursor
  [textures x y color]
  (cond (= color :b) (new-black-cursor textures x y)
        (= color :w) (new-white-cursor textures x y)))

(defn new-stone
  [textures x y color]
  (cond (= color :b) (new-black-stone textures x y)
        (= color :w) (new-white-stone textures x y)))
