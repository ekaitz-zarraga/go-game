(ns go.core
  (:require [play-clj.core  :refer :all]
            [play-clj.g2d   :refer :all]
            [play-clj.ui    :refer :all]
            [go.entities    :refer :all]
            [go.lib         :refer :all]))

; Just for testing
(def size 19)
(def tilesize 11)

(declare title-screen menu-screen game-screen overlay-screen)

(defn state-to-board
  ; TODO Create the entities to render from the logical state
  [textures state]
  (map #(apply new-stone textures (concat [] (key %) [(val %)]) ) (last state)))

(defn move-cursor
  ; TODO use the new entities model
  [screen [entities]]
    (let [textures (:textures entities)
          state    (:state entities)
          ui       (:ui entities)
          pos      (input->screen screen (:input-x screen)
                                         (:input-y screen))
          x        (int (/ (:x pos) tilesize))
          y        (int (/ (:y pos) tilesize)) ]
      (if (and (< x size) (< y size))
        (->> state
             color-from-hist
             (new-cursor textures x y)
             (assoc ui :cursor)
             (assoc entities :ui))
        [entities])))

(defscreen main-screen
  :on-show
  (fn [screen entities]
    (let [screen            (update! screen :renderer (stage)
                                            :camera   (orthographic))
          textures          (create-textures! tilesize)]
      ; TODO for ui and stuff, at the moment leave it to the board size
      ;(height! screen 256)
      (height! screen (* tilesize size))
      [{:textures textures
        :state    [{}]
        :ui
          {:board   (create-board size textures)
           :cursor  (new-cursor textures 0 0 :b)}
        }]))

  :on-render
  (fn [screen [entities]]
    (clear!)
    (let [textures (:textures entities)
          ui       (:ui entities)
          state    (:state entities)]
      ; Note: magic trick to use useful x and y values in the entities
      ; TODO HANDLE THE OFFSET OF THE BOTTOM-LEFT CORNER
      (render! screen (map #(assoc % :x (* tilesize (:x %))
                                     :y (* tilesize (:y %)))
                           (concat (:board ui)
                                   (state-to-board textures state)
                                   [(:cursor ui)])))
      [entities]))

  :on-touch-down
  (fn [screen [entities]]
    ; Pass through at the moment
    (let [state  (:state entities)
          color  (color-from-hist state)
          cursor (:cursor (:ui entities))
          pos    [(:x cursor) (:y cursor)]]
      [(->> state
            last
            (put-stone size color pos)
            (conj state)
            (assoc entities :state))] ))

  :on-mouse-moved move-cursor

  :on-resize
  (fn [screen [textures state background]]
    ; TODO for ui and stuff, at the moment leave it to the board size
    ;(height! screen 256)
    (height! screen (* tilesize size))))

(defgame go-game
  :on-create
  (fn [this]
    (set-screen! this main-screen )))


; NOTE: For development
(defscreen blank-screen
  :on-render
  (fn [screen entities]
    (clear!)))
(set-screen-wrapper! (fn [screen-atom screen-fn]
                       (try (screen-fn)
                         (catch Exception e
                           (.printStackTrace e)
                           (set-screen! go-game blank-screen)))))
