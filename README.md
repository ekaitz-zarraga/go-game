## Introduction

A play-clj game to play Go.
As simple as that.

## Contents

* ~~`android/src` Android-specific code~~
* `desktop/resources` Images, audio, and other files
* `desktop/src` Desktop-specific code
* `desktop/src-common` Cross-platform game code
